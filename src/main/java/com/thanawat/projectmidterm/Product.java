/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanawat.projectmidterm;

import java.io.Serializable;

/**
 *
 * @author THANAWAT_TH
 */
public class Product implements Serializable{
    private String ID;
    private String Name;
    private String Brand;
    private double Price;
    private int Amount;
    
    Product(String ID,String Name,String Brand,double price,int Amount){
        this.ID = ID;
        this.Name = Name;
        this.Brand = Brand;
        this.Price = price;
        this.Amount = Amount;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String Brand) {
        this.Brand = Brand;
    }

    public double getPrice() {      
        return Price;
    }

    public void setPrice(double price) {
        this.Price = price;
    }

    public int getAmount() {
        return Amount;
    }

    public void setAmount(int Amount) {
        this.Amount = Amount;
    }

    @Override
    public String toString() {
        return "ID : " + ID + " , Name = " + Name + " , Brand = " + Brand 
                + " , Price = " + Price + " , Amount = " + Amount;
    }
}
