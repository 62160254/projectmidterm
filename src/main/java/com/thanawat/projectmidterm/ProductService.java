/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanawat.projectmidterm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author THANAWAT_TH
 */
class ProductService implements Serializable{
    private static ArrayList<Product> ProductList = new ArrayList<Product>();
    static{
        load();
    }
    
    public static boolean addProduct(Product product){
        ProductList.add(product);
        save();
        return true;
    }
    public static boolean delProduct(int index){
        ProductList.remove(index);
        save();
        return true;
    }
    public static ArrayList<Product> getProduct(){
        
        return ProductList;
    }
    public static Product getProduct(int index){
        return ProductList.get(index);
    }
    public static boolean updateProduct(int index, Product product){
        ProductList.set(index, product);
        save();
        return true;
    }
    public static boolean clear(){
        ProductList.clear();
        save();
        return true;
    }
    static double Totalprice(){
        double Totalprice = 0.00 ;
        
         for (int i = 0; i < ProductList.size(); i++) {
            Totalprice += ProductList.get(i).getAmount() * ProductList.get(i).getPrice();
        }
        
        return Totalprice;
    }
    static int TotalAmount(){
        int TotalAmount = 0;
        
        for (int i = 0; i < ProductList.size(); i++) {
            TotalAmount += ProductList.get(i).getAmount();
        }
        
        return TotalAmount;
    }
    public static void save(){
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("Stang.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(ProductList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void load(){
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("Stang.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            ProductList = (ArrayList<Product>)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
}
